package com.tedcode.mvvmap

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {

    companion object{
       public var NOTE_ADDED = "new_note"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        button.setOnClickListener {
            var intent = Intent()
            if(TextUtils.isEmpty(editText.text)){
                setResult(Activity.RESULT_CANCELED)
            }else{
                var note = editText.text.toString()
                intent.putExtra(NOTE_ADDED,note)
                setResult(Activity.RESULT_OK,intent)
            }

            finish()
        }
    }
}
