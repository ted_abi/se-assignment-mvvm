package com.tedcode.mvvmap

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface NoteDao {
    @Insert
    fun insert(note:Note)

    @Query("select * from notes")
    fun getAllNotes() : LiveData<List<Note>>
}